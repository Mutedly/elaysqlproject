-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 04, 2023 at 02:00 AM
-- Server version: 10.4.24-MariaDB
-- PHP Version: 8.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `webuildsqlproj`
--

-- --------------------------------------------------------

--
-- Table structure for table `domains`
--

CREATE TABLE `domains` (
  `domain` varchar(50) NOT NULL,
  `userID` tinyint(255) DEFAULT NULL,
  `username` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `domains`
--

INSERT INTO `domains` (`domain`, `userID`, `username`) VALUES
('https://www.demo.extension', 9, 'demo user'),
('https://www.elay.py', 1, 'elay'),
('https://www.evlin.dev', 8, 'evlin'),
('https://www.noa.io', 7, 'noa'),
('https://www.reuven.net', 5, 'reuven'),
('https://www.ron.org', 2, 'ronnen'),
('https://www.shimon.com', 4, 'shimon'),
('https://www.shlomit.xyz', 6, 'shlomit'),
('https://www.shlomo.co.il', 3, 'shlomo');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `roleID` tinyint(255) NOT NULL,
  `roleName` varchar(30) DEFAULT 'צוות ללא שם',
  `userID` tinyint(255) NOT NULL,
  `username` varchar(10) NOT NULL,
  `permLevel` tinyint(3) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`roleID`, `roleName`, `userID`, `username`, `permLevel`) VALUES
(1, 'System Administrator', 1, 'elay', 15),
(2, 'User', 2, 'ronnen', 0),
(3, 'צוות ללא שם', 3, 'shlomo', 0),
(4, 'Manager', 4, 'shimon', 13),
(5, 'Developer', 5, 'reuven', 12),
(6, 'Designer', 6, 'shlomit', 1),
(7, 'צוות ללא שם', 7, 'noa', 5),
(8, 'Moderator', 8, 'evlin', 10),
(9, 'צוות ללא שם', 9, 'demo user', 0);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `userID` tinyint(255) NOT NULL,
  `username` varchar(10) NOT NULL,
  `email` varchar(30) NOT NULL,
  `password` varchar(20) NOT NULL,
  `isBanned` varchar(5) DEFAULT '0',
  `balance` decimal(5,3) NOT NULL,
  `bankAccount` tinyint(255) NOT NULL,
  `bankBranch` tinyint(255) NOT NULL,
  `bankNumber` tinyint(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`userID`, `username`, `email`, `password`, `isBanned`, `balance`, `bankAccount`, `bankBranch`, `bankNumber`) VALUES
(1, 'elay', 'elay@domain.com', '123', '0', '20.300', 1, 12, 13),
(2, 'ronnen', 'ron@domain.com', '456', '0', '10.330', 2, 13, 12),
(3, 'shlomo', 'shlomo@domain.com', '678', '0', '13.100', 3, 14, 15),
(4, 'shimon', 'shim@domain.com', '789', '0', '60.400', 4, 12, 10),
(5, 'reuven', 'reu@domain.com', '8910', '0', '10.700', 5, 12, 11),
(6, 'shlomit', 'shlomit@domain.com', '91011', '0', '30.900', 6, 10, 17),
(7, 'noa', 'noa@domain.com', '101112', '0', '50.990', 7, 11, 19),
(8, 'evlin', 'evlin@domain.com', '111213', '1', '10.990', 8, 12, 31),
(9, 'demo user', 'demo@domain.com', '354213', '0', '20.190', 9, 12, 21);

-- --------------------------------------------------------

--
-- Table structure for table `websites`
--

CREATE TABLE `websites` (
  `creationDate` timestamp NOT NULL DEFAULT current_timestamp(),
  `hostPrice` decimal(5,2) NOT NULL,
  `userID` tinyint(255) DEFAULT NULL,
  `username` varchar(10) DEFAULT NULL,
  `domain` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `websites`
--

INSERT INTO `websites` (`creationDate`, `hostPrice`, `userID`, `username`, `domain`) VALUES
('2022-11-29 23:53:24', '100.00', 9, 'demo user', 'https://www.demo.extension'),
('2022-11-29 23:53:24', '120.79', 1, 'elay', 'https://www.elay.py'),
('2022-11-29 23:53:24', '80.00', 8, 'evlin', 'https://www.evlin.dev'),
('2022-11-29 23:53:24', '96.33', 7, 'noa', 'https://www.noa.io'),
('2022-11-29 23:53:24', '71.99', 5, 'reuven', 'https://www.reuven.net'),
('2022-11-29 23:53:24', '112.28', 2, 'ronnen', 'https://www.ron.org'),
('2022-11-29 23:53:24', '88.72', 4, 'shimon', 'https://www.shimon.com'),
('2022-11-29 23:53:24', '78.64', 6, 'shlomit', 'https://www.shlomit.xyz'),
('2022-11-29 23:53:24', '79.99', 3, 'shlomo', 'https://www.shlomo.co.il');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `domains`
--
ALTER TABLE `domains`
  ADD PRIMARY KEY (`domain`),
  ADD KEY `userID` (`userID`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`roleID`),
  ADD UNIQUE KEY `username` (`username`),
  ADD KEY `userID` (`userID`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`userID`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `bankAccount` (`bankAccount`);

--
-- Indexes for table `websites`
--
ALTER TABLE `websites`
  ADD PRIMARY KEY (`domain`),
  ADD UNIQUE KEY `domain` (`domain`),
  ADD KEY `userID` (`userID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `roleID` tinyint(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `userID` tinyint(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `domains`
--
ALTER TABLE `domains`
  ADD CONSTRAINT `domains_ibfk_1` FOREIGN KEY (`userID`) REFERENCES `users` (`userID`);

--
-- Constraints for table `roles`
--
ALTER TABLE `roles`
  ADD CONSTRAINT `roles_ibfk_1` FOREIGN KEY (`userID`) REFERENCES `users` (`userID`);

--
-- Constraints for table `websites`
--
ALTER TABLE `websites`
  ADD CONSTRAINT `websites_ibfk_1` FOREIGN KEY (`userID`) REFERENCES `users` (`userID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
